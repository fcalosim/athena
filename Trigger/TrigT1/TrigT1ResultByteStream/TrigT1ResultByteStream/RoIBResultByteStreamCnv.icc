// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGT1RESULTBYTESTREAM_ROIBRESULTBYTESTREAMCNV_ICC
#define TRIGT1RESULTBYTESTREAM_ROIBRESULTBYTESTREAMCNV_ICC

// Gaudi/Athena include(s):
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/IRegistry.h"

#include "ByteStreamCnvSvcBase/ByteStreamAddress.h"
#include "ByteStreamCnvSvcBase/ByteStreamAddressL1R.h"
#include "ByteStreamData/RawEvent.h"

#include "SGTools/StorableConversions.h"

// Trigger include(s):
#include "TrigT1Result/RoIBResult.h"

// Local include(s):
#include "TrigT1ResultByteStream/L1SrcIdMap.h"

/**
 * The constructor sets up all the ToolHandle and ServiceHandle objects and initialises the
 * base class in the correct way.
 */
template< class ROBF >
RoIBResultByteStreamCnv< ROBF >::RoIBResultByteStreamCnv( ISvcLocator* svcloc )
  : Converter( storageType(), classID(), svcloc ),
    m_tool( "RoIBResultByteStreamTool" ),
    m_ByteStreamCnvSvc( "ByteStreamCnvSvc", "RoIBResultByteStreamCnv" ) {

}

/**
 * Function telling the framework the Class ID of the object that this converter
 * is for (RoIBResult).
 */
template< class ROBF >
const CLID& RoIBResultByteStreamCnv< ROBF >::classID(){
  return ClassID_traits< ROIB::RoIBResult >::ID();
}

template< class ROBF >
long RoIBResultByteStreamCnv< ROBF >::storageType() {
  return ByteStreamAddress::storageType();
}

/**
 * Init method gets all necessary services etc.
 */
template< class ROBF >
StatusCode RoIBResultByteStreamCnv< ROBF >::initialize() {

  //
  // Initialise the base class:
  //
  ATH_CHECK( Converter::initialize() );

  MsgStream log( msgSvc(), "RoIBResultByteStreamCnv" );
  log << MSG::DEBUG << "In initialize()" << endmsg;

  //
  // Get ByteStreamCnvSvc:
  //
  ATH_CHECK( m_ByteStreamCnvSvc.retrieve() );
  log << MSG::DEBUG << "Connected to ByteStreamCnvSvc" << endmsg;

  //
  // Get RoIBResultByteStreamTool:
  //
  ATH_CHECK( m_tool.retrieve() );
  log << MSG::DEBUG << "Connected to RoIBResultByteStreamTool" << endmsg;

  return StatusCode::SUCCESS;
}

/**
 * This function creates the RoIBResult object from the BS data. It collects all the
 * ROB fragments coming from the RoI Builder and gives them to RoIBResultByteStreamTool
 * for conversion.
 */
template< class ROBF >
StatusCode RoIBResultByteStreamCnv< ROBF >::createObj( IOpaqueAddress* pAddr, DataObject*& pObj ) {

  MsgStream log( msgSvc(), "RoIBResultByteStreamCnv" );
  log << MSG::DEBUG << "createObj() called" << endmsg;

  ByteStreamAddressL1R *pAddrL1;
  pAddrL1 = dynamic_cast< ByteStreamAddressL1R* >( pAddr );
  if( ! pAddrL1 ) {
    log << MSG::ERROR << " Cannot cast to ByteStreamAddressL1R" << endmsg ;
    return StatusCode::FAILURE;
  }

  ROIB::RoIBResult* result = new ROIB::RoIBResult;

  const std::vector< ROBF >& robs = pAddrL1->getPointers();

  // Convert to Object
  ATH_CHECK( m_tool->convert( robs, result ) );

  log << MSG::DEBUG << " Created Objects: " << *( pAddrL1->par() ) <<endmsg;

  pObj = SG::asStorable( result ) ;
  return StatusCode::SUCCESS;

}

/**
 * This function receives an RoIBResult object as input, and adds all the ROB fragments
 * of the RoI Builder to the current raw event using the IByteStreamEventAccess
 * interface.
 */
template< class ROBF >
StatusCode RoIBResultByteStreamCnv< ROBF >::createRep( DataObject* pObj, IOpaqueAddress*& pAddr ) {

  MsgStream log( msgSvc(), "RoIBResultByteStreamCnv" );
  log << MSG::DEBUG << "createRep() called" << endmsg;

  RawEventWrite* re = m_ByteStreamCnvSvc->getRawEvent();

  ROIB::RoIBResult* result;

  if( ! SG::fromStorable( pObj, result ) ) {
    log << MSG::ERROR << " Cannot cast to RoIBResult" << endmsg ;
    return StatusCode::FAILURE;
  }

  ByteStreamAddress* addr = new ByteStreamAddress( classID(), pObj->registry()->name(), "" );

  pAddr = addr;

  return m_tool->convert( result, re );

}

#endif // TRIGT1RESULTBYTESTREAM_ROIBRESULTBYTESTREAMCNV_ICC
